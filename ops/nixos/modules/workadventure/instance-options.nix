# Configuration options specific to a single workadventure instance.

{ lib, config, pkgs, ... }:

with lib;

{
  options = rec {
    backend = {
      httpPort = mkOption {
        default = 8081;
        type = types.ints.u16;
        description = "The TCP port the backend will bind to for http";
      };

      grpcPort = mkOption {
        default = 50051;
        type = types.ints.u16;
        description = "The TCP port the backend will bind to for grpc";
      };

      package = mkOption {
        default = pkgs.workadventure.back;
        defaultText = "pkgs.workadventure.back";
        type = types.package;
        description = "Backend package to use";
      };
    };

    pusher = {
      port = mkOption {
        default = 8080;
        type = types.ints.u16;
        description = "The TCP port the pusher will bind to";
      };

      package = mkOption {
        default = pkgs.workadventure.pusher;
        defaultText = "pkgs.workadventure.pusher";
        type = types.package;
        description = "Pusher package to use";
      };
    };

    frontend = {
      package = mkOption {
        default = pkgs.workadventure.front;
        defaultText = "pkgs.workadventure.front";
        type = types.package;
        description = "Front package to use";
      };

      urls = {
        api = mkOption {
          default = "/pusher";
          type = types.str;
          description = "The base url for the api, from the browser's point of view";
        };

        uploader = mkOption {
          default = "/uploader";
          type = types.str;
          description = "The base url for the uploader, from the browser's point of view";
        };

        admin = mkOption {
          default = "/admin";
          type = types.str;
          description = "The base url for the admin, from the browser's point of view";
        };

        maps = mkOption {
          default = "/maps";
          type = types.str;
          description = "The base url for serving maps, from the browser's point of view";
        };
      };
    };

    maps = {
      package = mkOption {
        default = pkgs.workadventure.maps;
        defaultText = "pkgs.workadventure.maps";
        type = types.package;
        description = "Maps package to use";
      };
    };

    nginx = {
      default = mkOption {
        default = false;
        type = types.bool;
        description = "Whether this instance will be the default one served by nginx";
      };

      domain = mkOption {
        default = null;
        type = types.nullOr types.str;
        description = "The domain name to serve workadenture services under. Mutually exclusive with domains.X";
      };

      serveDefaultMaps = mkOption {
        default = true;
        type = types.bool;
        description = "Whether to serve the maps provided by workadventure";
      };

      domains = {
        back = mkOption {
          default = null;
          type = types.nullOr types.str;
          description = "The domain name to serve the backend under";
        };

        pusher = mkOption {
          default = null;
          type = types.nullOr types.str;
          description = "The domain name to serve the pusher under";
        };

        maps = mkOption {
          default = null;
          type = types.nullOr types.str;
          description = "The domain name to serve the maps under";
        };

        front = mkOption {
          default = null;
          type = types.nullOr types.str;
          description = "The domain name to serve the front under";
        };
      };
    };
  };
}
