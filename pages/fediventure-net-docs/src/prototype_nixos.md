# Install on NixOS

This describes how to run the Fediventure Prototype on NixOS hosts. This is a production-ready deployment that is the recommended way of running the Prototype.

**Note**: this will **only work on NixOS hosts**, not Nix installed on other distributions / operating systems.

## Licensing

The Fediventure code is licensed under the GNU AGPL 2.0, and it bases on WorkAdventure, which itself is license under the GNU AGPL 2.0 with The Commons Clause. **Make sure you understand what this entails**.

The authors' interpretation with regards to Nix(OS) code is that imports from your configuration **are derivative works** of the Fediventure NixOS modules and other Nix code.

However, as the Nix(OS) **configuration** only executes at system configuration build time, and is not part of the served Fediventure instance, **you do not need to publish it**.

On the other hand, **any changes to the code that runs within Fediventure services will need to be published** - be it WorkAdventure components or wrapper scripts within Nix derivations. Keep in mind the Commons Clause condition that WorkAdventure is distributed with.

## Prerequisites

1) A machine running NixOS with publicly reachable 80/443 ports (or an equivalent reverse proxy setup)
2) A domain pointed to the machine (currently we only support running all components of the Prototype under a single domain name).

In the rest of this document, we will show you how to install the Prototype to serve Fediventure on `adventure.example.com`.

## Creating a Fediventure module file

First, prepare a new NixOS module file for your fediventure deployment. This makes it easier to follow this guide.

    $ vi /etc/nixos/fediventure.nix

With the following content:

    { config, pkgs, ... }: {
       # Empty...
    }

And import it somewhere within your main `configuration.nix`:

    $ vi /etc/nixos/configuration.nix
    [...]
    {
      imports = [
        # Include the result of the hardware scan.
        ./hardware-configuration.nix
        # Include the fediventure config.
        ./fediventure.nix
      ];
   [...]

Now, `nixos-rebuild switch` should succeed, and the result should be a no-op.

## Importing the Fediventure git repository

As the Fediventure code lies within it's own repository, you will need to import it:


    $ vi /etc/nixos/fediventure.nix

    { config, pkgs, ... }: let
      fediventure = builtins.fetchGit {
        url = "https://gitlab.com/fediventure/fediventure.git";
        rev = "03b9a1ab6a7b0e56fcd2ce99970c762801e02e64";
      };
      sources = import "${fediventure}/nix/sources.nix";
      workadventure-nix = import (sources.workadventure-nix + "/overlay.nix");
    in {
      imports = [
        "${fediventure}/ops/nixos/modules/workadventure/workadventure.nix"
      ];
      nixpkgs.overlays = [
        workadventure-nix
      ];
    }

This performs a bunch of Import-From-Derivation magic to overlay [workadventure-nix](https://gitlab.com/fediventure/workadventure-nix/) onto your system nixpkgs and to import fediventure NixOS module definitions.

`nixos-rebuild switch` should continue to switch and be a no-op, but you should be able to also add `pkgs.workadventure.back` to your `environment.systemPackages` if you want to check that everything works.

## Configuring Fediventure

Now you're ready to start the workadventure services! In your configuration (either in `fediventure.nix` next to `nixpkgs.overlays`, or anywhere in your `configuration.nix` set the following:

    services.nginx.virtualHosts."adventure.example.com" = {
      enableACME = true;
      forceSSL = true;
    };

    services.workadventure.instances."adventure.example.com" = {
      nginx.domain = "adventure.example.com";
    };

Configure nginx/ACME accordingly - the above configuration should work fine for publicly-facing hosts.

`nixos-rebuild switch` and that's it, you should be able to visit https://adventure.example.com in your browser.

## Custom maps

By default, the instance will serve the default WorkAdventure maps from upstram at `/maps`. To disable this behaviour:

    services.workadventure.instances."adventure.example.com" = {
      # previously set options ...
      nginx.serveDefaultMaps = false;
    };

To configure which map to load by default, set:

    services.workadventure.instances."adventure.example.com" = {
      # previously set options ...
      frontend.urls.maps = "https://maps.example.com/";
    };

The client will then load `https://maps.example.com/Floor0/floor0.json` by default. You will have to set up `maps.example.com` yourself to serve the map static files, with appropriate CORS headers set to allow access from your WA domain.

TODO(q3k): make `Floor0/floor0.json` configurable!

TODO(q3k): describe map serving on NixOS
