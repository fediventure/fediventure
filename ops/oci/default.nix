{ fediventure, lib, pkgs, ... }:

let
  # Creates a tini-based wrapper script to properly handle signals when
  # workadventure runs in a container
  buildLauncher = binaryName:
    (with pkgs;
      writeScript "run.sh" ''
        #!${runtimeShell}
        exec ${tini}/bin/tini -- ${binaryName} "$@"
      '');

  buildContainer = package: binaryName:
    pkgs.dockerTools.buildLayeredImage {
      # We used binaryName instead of package.pname here since both upstream
      # packages have the same name internally...
      name = "${binaryName}";
      tag = "latest";
      config.Cmd = [ (buildLauncher "${package}/bin/${binaryName}") ];
      config.User = "65534";
    };
  wapkgs = pkgs.workadventure;
in rec {
  containers = {
    back = buildContainer wapkgs.back "workadventureback";
    pusher = buildContainer wapkgs.pusher "workadventurepusher";
    front = let
      nginxConf = pkgs.writeText "nginx.conf" ''
        user nobody nobody;
        daemon off;
        error_log /dev/stdout info;
        pid /dev/null;
        events {}
        http {
          include ${pkgs.nginx}/conf/mime.types;
          access_log /dev/stdout;

          server {
            listen 8000;
            index index.html;
            root /var/www;

            location /_/ { try_files /index.html =404; }
            location /pusher/ { proxy_pass http://pusher:8080/; }
          }
        }
      '';
    in
    pkgs.dockerTools.buildLayeredImage {
      name = "workadventurefront";
      tag = "latest";
      contents = [
        pkgs.dockerTools.fakeNss
        pkgs.nginx
      ];

      extraCommands = ''
      # nginx still tries to read this directory even if error_log
      # directive is specifying another file :/
        mkdir -p var/log/nginx
        mkdir -p var/cache/nginx
        cp -r ${pkgs.workadventure.front.override {
          environment = {
            API_URL = "/pusher";
            MAPS_URL = "/maps";
          };
        }}/dist var/www
      '';

      config = {
        Cmd = [ "nginx" "-c" nginxConf ];
        ExposedPorts = {
          "8000/tcp" = {};
        };
      };
    };
  };

  # This script loads all built containers into currently running docker daemon
  # for testing
  load-containers = pkgs.writeScript "load-containers" ''
    #!${pkgs.runtimeShell}
    set -e
    ${lib.concatStringsSep "\n" (lib.mapAttrsToList (containerName: container:
      ''
        echo "Loading ${containerName}..."
        docker load -i ${container}'')
      containers)}
  '';

  # This script loads all built containers and runs docker-compose test
  # deployment.
  load-and-test = pkgs.writeScript "load-and-test" ''
    ${load-containers}
    docker-compose -f ${./docker-compose.yml} up
  '';

  # This script pushes built containers into container registry. It accepts some
  # configuration via environment variables:
  #   * REGISTRY_PREFIX - prefix for target image name (eg. "registry.gitlab.com/orga/project/")
  #   * REGISTRY_TAGS - target image tags, colon-separated
  #   * REGISTRY_CREDS - username:password registry authentication pair
  push-containers = pkgs.writeScript "push-containers" (let
    mustGetEnv = k: let
      v = builtins.getEnv k;
    in if v == "" then builtins.throw "${k} must be set" else v;

    prefix = mustGetEnv "REGISTRY_PREFIX";
    tags = lib.splitString ":" (mustGetEnv "REGISTRY_TAGS");
    creds = mustGetEnv "REGISTRY_CREDS";
  in ''
    #!${pkgs.runtimeShell}
    set -e
    ${lib.concatStringsSep "\n" (lib.mapAttrsToList (containerName: container:
      (lib.concatStringsSep "\n" (map (tag: 
      ''
        echo "Pushing ${prefix}${container.imageName}:${tag}..."
        ${pkgs.skopeo}/bin/skopeo \
          --insecure-policy \
          --dest-creds "${creds}" \
          copy \
          docker-archive://${container} "docker://${prefix}${container.imageName}:${tag}"
      '') tags))
      ) containers)}
  '');
}
