{ fediventure, lib, pkgs, stdenv, ... }:

let
  cleanTargetFilter = name: type: !(type == "directory" && baseNameOf (toString name) == "target");
  cleanTarget = src: lib.cleanSourceWith { filter = cleanTargetFilter; inherit src; };
in

pkgs.rustPlatform.buildRustPackage rec {
  pname = "fediventure-gateway";
  version = "0.1.0";

  src = lib.cleanSource (cleanTarget ./.);

  cargoSha256 = "0vscsakmb642322723c7iwqmjssvfa5vhy3wckfk01r3d49ql6w0";

  PROTOC = "${pkgs.protobuf}/bin/protoc";
  PROTO_PATH = "${fediventure.proto}";

  meta = with stdenv.lib; {
    description = "Fediventure gateway";
    homepage = "https://gitlab.com/fediventure/fediventure";
    license = licenses.agpl3;
  };
}
