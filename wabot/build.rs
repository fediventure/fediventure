fn main() -> std::io::Result<()> {
  prost_build::compile_protos(&["../proto/messages.proto"], &["../proto/"])?;
  Ok(())
}
