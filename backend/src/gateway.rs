use std::net::SocketAddr;
use tonic::{Request, Streaming, metadata::MetadataMap};
use tokio::sync::mpsc::UnboundedSender;
use log::info;

use crate::fediventure;

pub struct GatewayConnection {
    addr: Option<SocketAddr>,
    request_metadata: MetadataMap,
    tx: UnboundedSender<fediventure::Any>,
}

impl GatewayConnection {
    pub fn new(request: &Request<Streaming<fediventure::Any>>, tx: UnboundedSender<fediventure::Any>) -> Self {
        GatewayConnection {
            addr: request.remote_addr(),
            request_metadata: request.metadata().clone(),
            tx
        }
    }

    pub fn send(&self, msg: fediventure::Any) {
        if let Err(e) = self.tx.send(msg) {
            // It's not like we can do anything better here.
            info!("Error pushing message to the gateway. This shouldn't happen: {:#}", e);
        }
    }

    pub fn metadata(&self) -> &MetadataMap {
        &self.request_metadata
    }

    pub fn gateway_adr(&self) -> &Option<SocketAddr> {
        &self.addr
    }
}
