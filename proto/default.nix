{ fediventure, pkgs, stdenv, ... }:

derivation {
  system = builtins.currentSystem;
  name = "fediventure-proto";
  builder = "${pkgs.bash}/bin/bash";
  args = [ "-c" "${pkgs.coreutils}/bin/cp -r $src $out/"];

  src = ./.;
}
