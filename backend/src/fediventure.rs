use std::convert::TryFrom;
use thiserror::Error;
use prost::Message as ProstMessage;
use lazy_static::lazy_static;

tonic::include_proto!("fediventure");
tonic::include_proto!("fediventure.player");
tonic::include_proto!("fediventure.player.movement");

pub static REQUIRED_EXTENSIONS: [&'static str; 2] = ["player", "playerMovement"];
pub static OPTIONAL_EXTENSIONS: [&'static str; 0] = [];

#[derive(Debug, Error)]
pub enum MessageDecodingError {
    #[error("Unsupported message type. Message id: {0}")]
    UnsupportedMessageType(i32),
    #[error("Error decoding message:")]
    DecodeError(#[from] prost::DecodeError),
}

macro_rules! into_any {
    ($msg:ty, $id:literal) => {
        impl From<$msg> for Any {
            fn from(msg: $msg) -> Self {
                let mut buf = Vec::with_capacity(msg.encoded_len());
                msg.encode(&mut buf).unwrap();
                Any {
                    id: $id,
                    message: buf
                }
            }
        }
    };
}

macro_rules! id_map {
    ($($id:literal => $ext:ident :: $msg:ident),+) => {
        #[derive(Debug)]
        pub enum Message {
            $(
                $msg($msg)
            ),+
        }

        impl TryFrom<Any> for Message {
            type Error = MessageDecodingError;

            fn try_from(msg: Any) -> Result<Self, Self::Error> {
                let buf = msg.message.as_slice();
                Ok(match msg.id {
                    $(
                    $id => Message::$msg($msg::decode(buf)?),
                    )+
                    id => Err(MessageDecodingError::UnsupportedMessageType(id))?
                })
            }
        }

        $(into_any!($msg, $id);)+

        lazy_static! {
            pub static ref MESSAGE_IDS: Vec<MessageId> = {vec![$(
                MessageId {
                    id: $id,
                    name: stringify!($msg).to_string(),
                    extension: stringify!($ext).to_string()
                }
            ),+]};
        }
    }
}

id_map! {
    1 => player::JoinPlayer,
    2 => player::PlayerJoined,
    3 => player::PlayerLeft,
    4 => playerMovement::PlayerMoved
}
