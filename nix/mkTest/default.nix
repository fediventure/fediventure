{ pkgs, ...}:

# A simplistic test harness. Doing `mkdir $out` makes the test pass. The
# resulting derivation will be populated with an 'ok' file to indicate
# success.
#
# fediventure.nix.mkTest {
#   name = "foo";
#   buildCommand = ''
#     mkdir $out
#   '';
# }

args: derivation ({
  system = builtins.currentSystem;
  builder = "${pkgs.bash}/bin/bash";
  PATH = "${pkgs.coreutils}/bin";
  args = [ "-e" (builtins.toFile "builder-${args.name}.sh" ''
    eval "$buildCommand"
    [ -d $out ] && (echo "PASSED" ; touch $out/ok) || echo "FAILED"
  '')];
} // args)
