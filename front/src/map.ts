import axios from 'axios';

interface MapLayer {
  x: number
  y: number
  width: number
  height: number
  id: number
  name: string
  opacity: 1
  type: string
  visible: true
  data: number[]
}

interface TileProperty {
  name: string
  type: string
  value: unknown
}

interface Tile {
  id: number
  properties: TileProperty[]
}

interface TileSetConfig {
  columns: number
  firstgid: number
  image: string
  imagewidth: number
  imageheight: number
  margin: 1
  name: string
  spacing: 2
  tilecount: number
  tilewidth: number
  tileheight: number
  tiles: Tile[]
}

interface MapConfig {
  layers: MapLayer[]
  tilewidth: number
  tileheight: number
  tilesets: TileSetConfig[]
}

class TileSet {
  public image: HTMLImageElement
  public config: TileSetConfig
  private ready: Promise<void>

  constructor(config: TileSetConfig) {
    this.config = config;
    this.image = new Image();
    this.image.crossOrigin = "anonymous";
    this.image.src = config.image;
    this.ready = new Promise((resolve) => { this.image.onload = () => resolve() });
  }

  isMyId(id: number): boolean {
    return id >= this.config.firstgid && id < this.config.firstgid + this.config.tilecount;
  }

  async drawTile(id: number, dest: CanvasRenderingContext2D, x: number, y: number) {
    await this.ready;
    id -= this.config.firstgid;
    dest.drawImage(
      this.image,
      this.config.margin + (id%this.config.columns)*(this.config.tilewidth + this.config.spacing),
      this.config.margin + Math.floor(id/this.config.columns)*(this.config.tileheight + this.config.spacing),
      this.config.tilewidth, this.config.tileheight,
      x, y,
      this.config.tilewidth, this.config.tileheight
    );
  }
}

export default class Map {
  public config: MapConfig
  public tilesets: TileSet[]
  public renderedLayers: HTMLImageElement[]

  constructor(config: MapConfig, base_url: string) {
    this.config = config;
    this.tilesets = [];
    this.renderedLayers = [];
    for(const tileset of config.tilesets) {
      tileset.image = (new URL(tileset.image, base_url)).toString();
      this.tilesets.push(new TileSet(tileset));
    }
  }

  static async download(url: string): Promise<Map> {
    const response = await axios.get(url);
    const data = response.data;
    return new Map(data, url);
  }

  async drawTile(id: number, dest: CanvasRenderingContext2D, x: number, y: number) {
    for(const tileset of this.tilesets) {
      if(tileset.isMyId(id)) {
        await tileset.drawTile(id, dest, x, y);
        return;
      }
    }
  }

  async renderLayer(layer_id: number): Promise<HTMLImageElement> {
    const layer = this.config.layers[layer_id];
    const layerCanvas = document.createElement('canvas');
    layerCanvas.width = layer.width * this.config.tilewidth;
    layerCanvas.height = layer.height * this.config.tileheight;
    const layerCtx = layerCanvas.getContext('2d')!;
    let i = 0;
    for(const tile of layer.data) {
      i += 1;
      if(tile == 0) continue;
      await this.drawTile(tile, layerCtx, (i%layer.width)*this.config.tilewidth, Math.floor(i/layer.width)*this.config.tileheight)
    };
    const img = new Image();
    img.width = layerCanvas.width * 2;
    img.height = layerCanvas.height * 2;
    layerCanvas.toBlob((blob) => {
      const url = URL.createObjectURL(blob);

      img.onload = () => URL.revokeObjectURL(url);
      img.src = url;
    });
    return img;
  }

  async getRendered(): Promise<HTMLElement> {
    const container = document.createElement('div');
    container.className = 'map';
    for(let i = 0; i < this.config.layers.length; i++) {
      if(this.config.layers[i].visible && this.config.layers[i].type == "tilelayer") {
        const layer = await this.renderLayer(i);
        this.renderedLayers.push(layer);
        container.appendChild(layer);
      }
    }
    return container;
  }
}
