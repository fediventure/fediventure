# Install using OCI (Docker) images

We provide the following OCI images:

- `registry.gitlab.com/fediventure/fediventure/workadventureback:main` - backend
- `registry.gitlab.com/fediventure/fediventure/workadventurefront:main` - frontend
- `registry.gitlab.com/fediventure/fediventure/workadventurepusher:main` - pusher

TODO(q3k): describe how to configure/deploy this
