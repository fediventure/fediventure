use warp::Filter;
use serde_json::json;

mod backend;
mod backend_registry;
mod session;
mod player;
mod fediventure;
use backend_registry::BackendRegistry;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let backend_registry = BackendRegistry::new();
    let backend_registry = warp::any().map(move || backend_registry.clone());

    let anonym_login = warp::path("anonymLogin").map(|| warp::reply::json(&json!({"token": "dummy"})));
    let verify = warp::path("verify").map(|| warp::reply::json(&json!({"success": true})));
    let room = warp::path("room")
        .and(backend_registry)
        .and(warp::ws())
        .and(warp::query::<session::CharacterData>())
        .map(|backend_registry: BackendRegistry, ws: warp::ws::Ws, character_data| {
            ws.on_upgrade(move |socket| session::start(socket, character_data, backend_registry))
        });

    let routes = verify.or(room).or(anonym_login);

    // TODO: Make this configurable and use listenfd
    warp::serve(routes).run(([0u8,0,0,0], 8081u16)).await;
}
