{ pkgs, fediventure, ... }:

let
  pages = fediventure.pages;

in with pkgs; {
  server = pages.mkLektorServer "fediventure.net-devserver" ./.;
  build = pages.mkLektorBuild "fediventure.net-build" ./.;
}
