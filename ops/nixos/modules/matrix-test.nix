# This NixOS module deployes a test Matrix instance using Synapse.
#
# It is not meant for production usage, or even to be colocated with
# other services.
#
# Once running, you can register a test user as follows:
#
#    USER=foobar
#    register_new_matrix_user -c /var/lib/matrix-secret.yaml -u $USER 'http://[::1]:8008'
#
# Then, you can access the matrix server on https://app.element.io

{ config, pkgs, ... }:
let
  fqdn = "${config.networking.hostName}.${config.networking.domain}";
  registrationSecretFile = "/var/lib/matrix-secret.yaml";
in {
  # Allow access to nginx/ACME.
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  # Add register_new_matrix_user &co to system PATH
  environment.systemPackages = with pkgs; [
    matrix-synapse
  ];

  # Generate a registration token for Synapse and stuff it in an extra config file.
  systemd.services.matrix-token-generator = {
    enable = true;
    before = [ "matrix.synapse.service" ];
    wantedBy = [ "matrix-synapse.service" ];
    serviceConfig = {
      Type = "oneshot";
    };
    script = ''
      if [ ! -f ${registrationSecretFile} ]; then
        echo "registration_shared_secret: $(${pkgs.pwgen}/bin/pwgen 32 1 | tr -d '\n')" > "${registrationSecretFile}"
      fi
    '';
  };

  # Run a database for synapse. It's not publicly exposed, so we're okay with a
  # throwaway password.
  services.postgresql = {
    enable = true;
    initialScript = pkgs.writeText "synapse-init.sql" ''
      CREATE ROLE "matrix-synapse" WITH LOGIN PASSWORD 'synapse';
      CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
        TEMPLATE template0
        LC_COLLATE = "C"
        LC_CTYPE = "C";
    '';
  };

  # Run synapse itself.
  services.matrix-synapse = {
    enable = true;
    server_name = fqdn;
    # Include generated registration_shared_secret config file.
    extraConfigFiles = [
      "/var/lib/matrix-secret.yaml"
    ];
    listeners = [
      {
        port = 8008;
        bind_address = "::1";
        type = "http";
        tls = false;
        x_forwarded = true;
        resources = [
          {
            names = [ "client" "federation" ];
            compress = false;
          }
        ];
      }
    ];
  };

  # Route things appropriately to point c2s/s2s clients to our synapse, and
  # actually expose the synapse.
  services.nginx = {
    enable = true;
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
    virtualHosts = {
      "${fqdn}" = {
        enableACME = true;
        forceSSL = true;

        locations."/.well-known/matrix/server".extraConfig =
          let
            server = { "m.server" = "${fqdn}:443"; };
          in ''
            add_header Content-Type application/json;
            return 200 '${builtins.toJSON server}';
          '';
        locations."/.well-known/matrix/client".extraConfig =
          let
            client = {
              "m.homeserver" = { "base_url" = "https://${fqdn}"; };
            };
          in ''
            add_header Content-Type application/json;
            add_header Access-Control-Allow-Origin *;
            return 200 '${builtins.toJSON client}';
          '';

        locations."/".extraConfig = ''
          return 404;
        '';

        locations."/_matrix" = {
          proxyPass = "http://[::1]:8008";
        };
      };
    };
  };
}
