use std::collections::HashMap;
use std::sync::Arc;

use log::{debug, info};
use prost::Message as ProstMessage;
use thiserror::Error;
use tokio::sync::mpsc::{self, UnboundedSender};
use tokio::sync::RwLock;
use tokio_stream::{wrappers::UnboundedReceiverStream, StreamExt};
use tonic::transport::{Channel, Endpoint};

use crate::backend_registry::BackendRegistry;
use crate::fediventure::{
    backend_client::BackendClient, extension::Presence, Any, ClientHello, Extension, IdMap,
    JoinPlayer, Message, MessageDecodingError, OPTIONAL_EXTENSIONS, REQUIRED_EXTENSIONS,
    PlayerJoined, PlayerMoved,
};
use crate::session::Session;
use crate::player::Player;

#[allow(non_camel_case_types)]
pub mod proto {
    tonic::include_proto!("wa");

    macro_rules! oneof_from {
        ($from:ident, $for:ident, $mod:ident) => {
            impl From<$from> for $for {
                fn from(src: $from) -> Self {
                    $for {
                        message: Some($mod::Message::$from(src)),
                    }
                }
            }
        };
    }

    macro_rules! oneof_from_batch {
        ($from:ident) => {
            impl From<$from> for ServerToClientMessage {
                fn from(src: $from) -> Self {
                    ServerToClientMessage {
                        message: Some(server_to_client_message::Message::BatchMessage(BatchMessage {
                            event: "".to_owned(),
                            payload: vec![
                                SubMessage {
                                    message: Some(sub_message::Message::$from(src)),
                                }
                            ]
                        }))
                    }
                }
            }
        };
    }
    oneof_from!(JoinRoomMessage, PusherToBackMessage, pusher_to_back_message);
    oneof_from!(RoomJoinedMessage, ServerToClientMessage, server_to_client_message);
    oneof_from!(
        UserMovesMessage,
        PusherToBackMessage,
        pusher_to_back_message
    );
    oneof_from_batch!(UserMovedMessage);
    oneof_from_batch!(UserJoinedMessage);
}

#[derive(Debug, Error)]
pub enum BackendError {
    #[error("")]
    Tonic(#[from] tonic::transport::Error),
    #[error("Error while connecting to backend: ")]
    InvalidUri(#[from] http::uri::InvalidUri),
    #[error("Grpc call failed: ")]
    GrpcStatus(#[from] tonic::Status),
    #[error("Error during sending data to the backend: ")]
    SendError(#[from] mpsc::error::SendError<Any>),
    #[error("Message error: ")]
    MessageError(#[from] MessageDecodingError),
}

#[derive(Debug)]
pub struct Backend {
    url: String,
    registry: BackendRegistry,
    id_map: IdMap,
    client_queue: RwLock<Vec<Arc<Session>>>,
    clients: RwLock<HashMap<i32, Arc<Session>>>,
    players: RwLock<HashMap<i32, Arc<RwLock<Player>>>>,
    connection: RwLock<BackendClient<Channel>>,
    backend_tx: UnboundedSender<Any>,
    pub tx: UnboundedSender<Message>,
}

impl Backend {
    pub async fn new(registry: BackendRegistry, url: &str) -> Result<Arc<Self>, BackendError> {
        let endpoint = Endpoint::from_shared(url.to_owned())?;
        let connection = RwLock::new(BackendClient::connect(endpoint).await?);
        let (backend_tx, backend_rx) = mpsc::unbounded_channel();
        let backend_rx = UnboundedReceiverStream::new(backend_rx);
        let (tx, rx) = mpsc::unbounded_channel();
        let rx = UnboundedReceiverStream::new(rx);

        let mut client_hello = ClientHello {
            supported_extensions: Vec::with_capacity(
                REQUIRED_EXTENSIONS.len() + OPTIONAL_EXTENSIONS.len(),
            ),
        };

        for &ext in &REQUIRED_EXTENSIONS {
            client_hello.supported_extensions.push(Extension {
                name: ext.to_owned(),
                presence: Presence::Required.into(),
            });
        }

        for &ext in &OPTIONAL_EXTENSIONS {
            client_hello.supported_extensions.push(Extension {
                name: ext.to_owned(),
                presence: Presence::Optional.into(),
            });
        }

        println!("Sending hello!");
        let server_hello = connection
            .write()
            .await
            .hello(client_hello)
            .await?
            .into_inner();
        println!("Hello sent!");
        let choosen_extensions = server_hello
            .extensions
            .into_iter()
            .map(|ext| ext.name)
            .collect();
        let id_map = IdMap::new(server_hello.message_ids, choosen_extensions);

        let backend = Arc::new(Backend {
            url: url.to_owned(),
            registry,
            connection,
            client_queue: RwLock::default(),
            clients: RwLock::default(),
            players: RwLock::default(),
            id_map,
            backend_tx,
            tx,
        });
        backend.clone().spawn_client_rx_task(rx);
        backend.clone().spawn_backend_rx_task(backend_rx);
        return Ok(backend);
    }

    fn spawn_client_rx_task(self: Arc<Self>, mut rx: UnboundedReceiverStream<Message>) {
        tokio::task::spawn(async move {
            while let Some(msg) = rx.next().await {
                self.on_client_message(msg).await;
            }
        });
    }

    fn spawn_backend_rx_task(self: Arc<Self>, rx: UnboundedReceiverStream<Any>) {
        tokio::task::spawn(async move {
            let mut connection = self.connection.write().await;
            let mut response = connection
                .communicate(tonic::Request::new(rx))
                .await
                .unwrap()
                .into_inner();
            info!("Receiving messages");
            loop {
                match response.message().await {
                    Ok(Some(msg)) => {
                        self.on_backend_message(msg).await;
                    }
                    Ok(None) => {
                        debug!("The backend has closed the connection.");
                        break;
                    }
                    Err(e) => {
                        info!("{:?}", e);
                        break;
                    }
                }
            }
        });
    }

    // We don't care whether sending the message was successful,
    // as we can't do anything if it isn't.
    #[allow(unused_must_use)]
    fn send_backend_message(&self, msg: impl Into<Message>) -> Result<(), MessageDecodingError> {
        let backend_msg = self.id_map.into_backend_msg(msg.into())?;
        self.backend_tx.send(backend_msg);
        Ok(())
    }

    async fn broadcast_client_message(&self, msg: proto::ServerToClientMessage, except: i32) {
        debug!("Broadcasting message: {:?}", msg);
        let mut data = Vec::with_capacity(msg.encoded_len());
        // This is infallible if the buffer is large enough
        msg.encode(&mut data).unwrap();
        for client in self.clients.read().await.values() {
            if client.player.read().await.id == except {
                continue
            }
            // We really don't care whether the client received the mesasge.
            // After all, what do we do if they did not?
            #[allow(unused_must_use)]
            {
                client.send(&data);
            }
        }
    }

    async fn send_client_message(&self, client: &Arc<Session>, msg: proto::ServerToClientMessage) {
        debug!("Sending message to client {}: {:?}", client.player.read().await.id, msg);
        let mut data = Vec::with_capacity(msg.encoded_len());
        // This is infallible if the buffer is large enough
        msg.encode(&mut data).unwrap();
        // We really don't care whether the client received the mesasge.
        // After all, what do we do if they did not?
        #[allow(unused_must_use)]
        {
            client.send(&data);
        }
    }

    pub async fn connect_client(&self, client: Arc<Session>) -> Result<(), BackendError> {
        // Acquire lock here to ensure messages are sent
        // in the same order that clients appear in the Vec.
        let mut clients = self.client_queue.write().await;
        // Unwrap is fine, this is an required extension.
        self.send_backend_message(JoinPlayer {
            name: client.player.read().await.name.to_owned(),
        }).unwrap();
        clients.push(client);

        Ok(())
    }

    async fn my_player_joined(&self, client: Arc<Session>, id: i32) {
        client.player.write().await.id = id;
        let player = client.player.read().await;

        // Tell the backend where the player is.
        self.send_backend_message(PlayerMoved {
            id: id,
            x: player.x,
            y: player.y,
        }).unwrap();

        // Tell the client that it has successfully joined.
        self.send_client_message(&client, proto::RoomJoinedMessage {
            item: vec![],
            tag: vec![],
            current_user_id: id,
        }.into()).await;

        // Tell the client about everyone else
        for player in self.players.read().await.values() {
            let player = player.read().await;
            self.send_client_message(&client, proto::UserJoinedMessage {
                user_id: player.id,
                name: player.name.to_owned(),
                character_layers: vec![
                    proto::CharacterLayerMessage { url: "".to_owned(), name: "male1".to_owned() }
                ],
                position: Some(proto::PositionMessage {
                    x: player.x,
                    y: player.y,
                    direction: 0,
                    moving: false
                })
            }.into()).await;
        }
    }

    async fn on_backend_message(&self, msg: Any) {
        let id = msg.id;
        let msg = match self.id_map.from_backend_msg(msg) {
            Ok(msg) => msg,
            Err(_) => {
                info!(
                    "The backend at {} has sent us an invalid message (id {}). Ignoring it.",
                    self.url, id
                );
                return;
            }
        };

        match msg {
            Message::PlayerJoined(PlayerJoined { id, name }) => {
                let mut client_queue = self.client_queue.write().await;
                let mut pos = 0;
                for client in client_queue.iter() {
                    if client.player.read().await.name == name {
                        break
                    }
                    pos += 1;
                }

                let player = if pos < client_queue.len() {
                    let client = client_queue.remove(pos);
                    self.my_player_joined(client.clone(), id).await;
                    self.clients.write().await.insert(id, client.clone());
                    client.player.clone()
                } else {
                    Arc::new(RwLock::new(Player::new(name.to_owned(), 0, 0)))
                };

                let player_id = player.read().await.id;
                self.broadcast_client_message(proto::UserJoinedMessage {
                    user_id: player_id,
                    name: name.to_owned(),
                    character_layers: vec![
                        proto::CharacterLayerMessage { url: "".to_owned(), name: "male1".to_owned() }
                    ],
                    position: Some(proto::PositionMessage {
                        x: 0,
                        y: 0,
                        direction: 0,
                        moving: false
                    })
                }.into(), player_id).await;
                self.players.write().await.insert(id, player);
            },
            Message::PlayerMoved(PlayerMoved{ id, x, y }) => {
                if let Some(player) = self.players.read().await.get(&id) {
                    let mut player = player.write().await;
                    player.x = x;
                    player.y = y;
                }

                self.broadcast_client_message(proto::UserMovedMessage {
                    user_id: id,
                    position: Some(proto::PositionMessage {
                        x, y,
                        direction: 0,
                        moving: false
                    })
                }.into(), id).await;
            },
            _ => {
                // TODO: Implement other message types
            }
        }
    }

    async fn on_client_message(&self, msg: Message) {
        debug!("{:?}", msg);
        match msg {
            #[allow(unused_must_use)]
            Message::PlayerMoved(_) => {
                self.send_backend_message(msg);
            }
            _ => {
                // TODO: Implement other message types
            }
        }
    }
}
