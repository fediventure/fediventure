import axios from 'axios';
import Map from './map';
import Player from './player';
import Camera from './camera';

async function main() {
  const player = new Player(0, 0);
  const camera = new Camera(player);
  const map = await Map.download("https://gparant.github.io/tcm-client/TCM/Floor0/floor0.json");
  camera.content.appendChild(await map.getRendered());
  document.body.appendChild(camera.container);
  let direction = 0;
  let tick = () => {
    player.move(5*(+(direction==0) - +(direction==2)),5*(+(direction==1) - +(direction==3)));
    requestAnimationFrame(tick);
  }
  tick();
  setInterval(() => direction = (direction+1)%4, 1000);
}

main()
