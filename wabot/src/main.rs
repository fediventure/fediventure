use std::{thread, time};

use reqwest::blocking::Client;
use anyhow::Result;
use websocket::dataframe;
use prost::Message;

pub mod wa {
    include!(concat!(env!("OUT_DIR"), "/wa.rs"));
}
use wa::{ ServerToClientMessage, ClientToServerMessage, UserMovesMessage, PositionMessage, ViewportMessage, client_to_server_message };

#[derive(serde::Deserialize)]
#[allow(dead_code,non_snake_case)]
struct AuthResponse {
    authToken: String,
    userUuid: String
}

fn make_dataframe(data: Vec<u8>) -> dataframe::DataFrame {
    dataframe::DataFrame::new(true, dataframe::Opcode::Binary, data)
}

fn make_ping_dataframe() -> dataframe::DataFrame {
    make_dataframe(vec![])
}

fn run() -> Result<()> {
    let auth_response: AuthResponse = Client::builder().build()?.post("http://localhost/pusher/anonymLogin").send()?.json()?;
    let token = auth_response.authToken;

    let url = format!("ws://localhost/pusher/room?roomId=_%2Fglobal%2Fhttp:%2f%2flocalhost%2fmaps%2fFloor0%2ffloor0.json&token={}&name=wabot&characterLayers=male1&x=1392&y=688&top=365&bottom=1007&left=44&right=1472", "");

    let ws = websocket::ClientBuilder::new(&url)?.connect_insecure()?;
    let (mut receiver, mut sender) = ws.split()?;

    let offset_x: i32 = std::env::args().nth(1).unwrap().parse().unwrap();
    let offset_y: i32 = std::env::args().nth(2).unwrap().parse().unwrap();
    thread::spawn(move || {
        let mut i = 0;
        loop {
            i += 1;
            let message = UserMovesMessage {
                position: Some(PositionMessage {
                    x: offset_x*32 + 16 + 32 * ((i/2)%2),
                    y: offset_y*32 + 16 + 32 * (((i+1)/2)%2),
                    direction: 3-(i%4),
                    moving: false
                }),
                viewport: Some(ViewportMessage {
                    top: 0,
                    left: 0,
                    right: 1000,
                    bottom: 1000
                })
            };
            let mut encoded_message = vec![];
            ClientToServerMessage { message: Some(client_to_server_message::Message::UserMovesMessage(message))}.encode(&mut encoded_message).unwrap();
            sender.send_dataframe(&make_dataframe(encoded_message)).unwrap();
            thread::sleep(time::Duration::from_millis(100));
        }
    });
    
    for frame in receiver.incoming_dataframes() {
        let data = frame?.data;
        println!("{:?} {:?}", &data, ServerToClientMessage::decode(data.as_slice())?);
    }

    Ok(())
}

fn main() {
    run().unwrap();
}
