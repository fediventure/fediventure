# Summary

- [Overview](./overview.md)
- [Prototype](./prototype.md)
  - [Install on NixOS](./prototype_nixos.md)
  - [Install using OCI (Docker) images](./prototype_oci.md)
  - [Install from scratch](./prototype_from_scratch.md)
