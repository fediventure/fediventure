{ fediventure, lib, pkgs, stdenv, ... }:

let
  cleanTargetFilter = name: type: !(type == "directory" && baseNameOf (toString name) == "target");
  cleanTarget = src: lib.cleanSourceWith { filter = cleanTargetFilter; inherit src; };
in

pkgs.rustPlatform.buildRustPackage rec {
  pname = "fediventure-backend";
  version = "0.1.0";

  src = lib.cleanSource (cleanTarget ./.);

  cargoSha256 = "0kg87x1ma3g19rs59azbf157drmikmrvr1l7p8h0309gyw0rkj29";

  PROTOC = "${pkgs.protobuf}/bin/protoc";
  PROTO_PATH = "${fediventure.proto}";

  meta = with stdenv.lib; {
    description = "Fediventure backend";
    homepage = "https://gitlab.com/fediventure/fediventure";
    license = licenses.agpl3;
  };
}
