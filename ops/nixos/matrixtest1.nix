{ config, pkgs, ... }:

{
  imports = [
    ./modules/vm-kurdebele.nix
    ./modules/matrix-test.nix
  ];
  networking.hostName = "matrixtest1";
  networking.interfaces.enp1s0 = {
    ipv6.addresses = [
      { address = "2a01:4f9:4a:4319:1337::12"; prefixLength = 80; }
    ];
    ipv4.addresses = [
      { address = "135.181.235.220"; prefixLength = 29; }
    ];
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/06d70ee9-6cef-4922-afcf-4a0f4e03b24e";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/9049-7A28";
      fsType = "vfat";
    };
  };
  swapDevices = [
    { device = "/dev/disk/by-uuid/820fc945-d8fc-495c-ba4e-a722d2f31fba"; }
  ];
  system.stateVersion = "20.09";
}
