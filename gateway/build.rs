fn main() {
    let proto_path = std::env::var("PROTO_PATH").unwrap_or("../proto".to_owned());
    tonic_build::compile_protos(format!("{}/messages.proto", proto_path)).unwrap();
    tonic_build::compile_protos(format!("{}/fediventure.proto", proto_path)).unwrap();
    tonic_build::compile_protos(format!("{}/player.proto", proto_path)).unwrap();
    tonic_build::compile_protos(format!("{}/playerMovement.proto", proto_path)).unwrap();
}
