A basic Workadventure load testing bot. It connects to a pusher, generates a token, then connect to the local gateway (see the `gateway` directory) and moves around every 100ms.

How to use
----------

  1. Start WA
  2. Start gateway
  3. `cargo run 40 20` (the numbers are x and y coordinates in tiles, i.e. 40th tile from the left, 20th tile from the top)
