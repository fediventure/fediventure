export default class Player {
  public x: number
  public y: number
  public onmove?: () => void

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  move(dx: number, dy: number) {
    this.x += dx;
    this.y += dy;
    if(this.onmove) this.onmove();
  }
}
