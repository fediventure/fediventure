{ system ? builtins.currentSystem, ... }:

let
  sources = import ./nix/sources.nix;


  workadventure-nix = import (sources.workadventure-nix + "/overlay.nix");
  pkgs = import sources.nixpkgs { inherit system; overlays = [ workadventure-nix ]; };

  fix = f: let x = f x; in x;
  readTree = import ./nix/readTree {};

in fix (self: rec {
  config = {
    nixpkgs = pkgs;
    nixpkgsSrc = sources.nixpkgs;
    fediventure = self;
    root = ./.;
    inherit pkgs;
    inherit (pkgs) lib stdenv;
  };
  nix = readTree config ./nix;
  ops = readTree config ./ops;
  pages = readTree config ./pages;
  gateway = readTree config ./gateway;
  backend = readTree config ./backend;
  proto = readTree config ./proto;
  inherit pkgs;
})
