title: Home
---
body:

## Fediventure - The Federated Work Adventure for *Spaces

We're a group of people who are interested in extending the Work Adventure and the rC3 World experience as a fully federated network of Worlds from (hacker)spaces all around the world.

The current focus of the project is:

 - [Build a **federation specification**](/specifications)
 - Develop [**Nix(OS) modules**](https://gitlab.com/fediventure/fediventure/-/tree/main/ops/nixos/modules/workadventure) to run WorkAdventure in production for different *spaces.
 - Gather a community of *spaces that wish to run WA, and coordinate discussion between them.

Our main communication channel is [#fediventure:hackerspace.pl](https://matrix.to/#/#fediventure:hackerspace.pl?via=hackerspace.pl&via=matrix.org&via=tchncs.de) on Matrix. Our code is hosted at a public [GitLab project](https://gitlab.com/fediventure/fediventure).

We are cooperating with the company which created [Work Adventure](https://workadventu.re/): [The Coding Machine](https://www.thecodingmachine.com), and are aiming to **not** fork the project.

### I want to run a WA for our *space - what do I do?

You can now run the Fediventure prototype! See [fediventure.net/docs](https://fediventure.net/docs).
