{ pkgs, stdenv, ... }:

rec {
  inherit (pkgs.python38Packages) lektor;
  inherit (pkgs) mdbook;

  # mkLektorServer makes a script that will run a lektor development server
  # against a given lektor project. The path given will be leaked into an
  # absolute path on the filesystem (via builtins.toString) to ensure that the
  # dev server is run against a live version from the filesystem, and not a
  # copy within the nix store.
  mkLektorServer = name: path: pkgs.writeScriptBin name ''
    ${lektor}/bin/lektor --project=${builtins.toString path} server -v
  '';

  # mkLektorBuild returns a derivation that will build a given lektor project
  # into static files to be hosted via HTTP.
  mkLektorBuild = name: path: stdenv.mkDerivation {
    inherit name;

    buildCommand = ''
      mkdir -p $out
      ${lektor}/bin/lektor \
        --project=${path} \
        build \
        --output-path=$out
    '';
  };

  # mkMdbookServer makes a script that will run a mdbook development server
  # against a given mdbook project. The path given will be leaked into an
  # absolute path on the filesystem (via builtins.toString) to ensure that the
  # dev server is run against a live version from the filesystem, and not a
  # copy within the nix store.
  mkMdbookServer = name: path: pkgs.writeScript name ''
    ${mdbook}/bin/mdbook serve ${builtins.toString path}
  '';

  # mkLektorBuild returns a derivation that will build a given mdbook project
  # into static files to be hosted via HTTP.
  mkMdbookBuild = name: path: stdenv.mkDerivation {
    inherit name;

    buildCommand = ''
      mkdir -p $out
      ${mdbook}/bin/mdbook build ${path} -d $out
    '';
  };
}
