{ pkgs, fediventure, ... }:

let
  pages = fediventure.pages;

in with pkgs; {
  server = pages.mkMdbookServer "docs.fediventure.net-devserver" ./.;
  build = pages.mkMdbookBuild "docs.fediventure.net-build" ./.;
}
