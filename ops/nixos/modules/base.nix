{ config, pkgs, lib, fediventure, ... }:

{
  networking.domain = "i.fediventure.net";
  time.timeZone = "Europe/Amsterdam";
  networking.useDHCP = false;
  users.users.root = {
    openssh.authorizedKeys.keys = with fediventure.ops.admins; sshkeys groups.rootOnProd;
  };
  environment.systemPackages = with pkgs; [
    wget vim rxvt_unicode.terminfo git htop
  ];
  programs.mtr.enable = true;
  services.openssh.enable = true;

  security.acme.email = "q3k@hackerpace.pl";
  security.acme.acceptTerms = true;
}
