use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::Mutex;

use crate::backend::{Backend, BackendError};

#[derive(Debug, Default, Clone)]
pub struct BackendRegistry {
   backends: Arc<Mutex<HashMap<String, Arc<Backend>>>>
}

impl BackendRegistry {
    pub fn new() -> Self {
        BackendRegistry::default()
    }

    pub async fn get(&self, url: &str) -> Result<Arc<Backend>, BackendError> {
        // This program can't really do anything useful if the mutex is poisoned.
        // Let it crash in such case.
        let mut backends = self.backends.lock().await;
        if !backends.contains_key(url) {
            backends.insert(url.to_owned(), Backend::new(self.clone(), url).await?);
        }

        // Unwrap here is okay because we ensure the key exists two lines above
        return Ok(backends.get(url).unwrap().clone());
    }
}
