Operations
==========

This repository contains all operations and nixos-related code.

It currently contains a mix of NixOS modules specific to fediventure production, but also some generic workadventure modules. In the future these will be detangled from this directory, and instead be either split off to another directory, or upstreamed to nixpkgs.

Testing WorkAdventure modules via E2E test
------------------------------------------

The modules are defined in [nixos/modules/workdaventure](nixos/modules/workadventure).

The test is defined in [tests/workadventure-e2e.nix](tests/workadventure-e2e.nix).

To run the automated e2e tests:

    nix-build -A ops.tests.workadventure-e2e

To run the test manually:

    nix-build -A ops.tests.workadventure-e2e.driver
    # Then, run the test VM:
    result/bin/nixos-run-vms
    # and connect via VNC to 127.0.0.1:5900 (root with empty password).

    # ... or, you can experiment via the test driver:
    result/bin/nios-test-driver
    >>> start_all()
    >>> machine.execute("whoami")

If you want to connect to the machine via ssh, you can tell qemu to forward a port:

    nix-build -A ops.tests.workadventure-e2e.driver
    QEMU_NET_OPTS="hostfwd=tcp:127.0.0.1:2222-:22" result/bin/nixos-run-vms

Note that the host key will change each time, so you might want to do:

    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@localhost -p 2222

The password is `root`.
