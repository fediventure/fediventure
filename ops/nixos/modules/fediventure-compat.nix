# Compatibility NixOS module for using fediventure modules (from this
# repository) on NixOS. This makes the 'fediventure' argument be populated
# by NixOS modules that request it, and makes 'pkgs' point to the same
# 'pkgs' as fediventure's readTree pkgs arguments, ie. one overlaid with
# workadventure-nix.
#
# This is automatically handled for internal fediventure uses, ie. if you
# just want to run some NixOS tests or define NixOS configurations for
# fediventure-internal production, you don't need to include this module.
#
# TODO(q3k): figure this out for external users. Not yet sure if
# 'fediventure' is actually used by any public-facing modules, they seem
# to all be going through pkgs overlaying now, so users might not have to
# include this?

{ ... }: let

  fediventure = (import ../../../default.nix) {};

in {
  _module.args.fediventure = fediventure;
  nixpkgs.pkgs = fediventure.config.pkgs;
}
