#[derive(Debug)]
pub struct Player {
    pub id: i32,
    pub name: String,
    pub x: i32,
    pub y: i32,
}

impl Player {
    pub fn new(name: String, x: i32, y: i32) -> Self {
        Player {
            id: -1,
            name,
            x, y
        }
    }
}
