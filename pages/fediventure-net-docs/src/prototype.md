# Prototype

This approach to Fediventure aims to get things running across *spaces as soon as possible. It currently mostly consists of working on the WorkAdventure JavaScript frontend, and a bare minimum of changes to the existing backend services so that you can host your own instance easily.

Each deployment consists of the following:
    
                         .- - - - - - - - - - - - - - - - -.
        .-------------.  :                                 :
        | web browser |  :             .-------------.     :
        |-------------| <--- HTTP ---> | proxy       |     :
        |  frontend   |  :             | (eg. nginx) |     :
        '-------------'  :             '-------------'     :
                         :          .----'|      |         :
                         :          |   .-'      |         :
                         :          v   |        |         :
                         :  .----------------.   |         :
                         :  |      maps      |   |         :
                         :  | (static files) |   |         :
                         :  '----------------'   |         :
                         :              |        |         :
                         :              v        |         :
                         :    .----------------. |         :
                         :    |   frontend     | |         :
                         :    | (static files) | |         :
                         :    '----------------' |         :
                         :                   .---'         :
                         :                   v             :
                         :          .------------------.   :
                         :          |      pusher      |   :
                         :          |     (node.js)    |   :
                         :          '------------------'   :
                         :                   ^             :
                         :                   | gRPC        :
                         :                   v             :
                         :          .------------------.   :
                         :          |     backend      |   :
                         :          |     (node.js)    |   :
                         :          '------------------'   :
                         :                                 :
                         : adventure.example.com           :
                         '- - - - - - - - - - - - - - - - -'

So, in other terms, the Fediventure Prototype is a self-contained distribution of WorkAdventure, that contains a common patchset across all instances of it. As the Matrixful/Matrixless proposals evolve, the Prototype will too, and will become a fully fledged federated instance within the protocol that is picked.

## Installation

To install the Prototype, you can follow any of the following:

- [Install on NixOS](prototype_nixos.md), if this is what you're running on your production machine. This is the main recommended method of running Fediventure.
- [Install using OCI images](prorotype_oci.md), if running OCI images is your preferred production deployment approach (ie. if you want to use Docker/Kubernetes).
- [Install from scratch](prototype_from_scratch.md), if the above does not work for you. This is **highly discouraged**, instead we would prefer [hearing from you](https://gitlab.com/fediventure/fediventure/-/issues/new) why none of the above work for you.
