use std::sync::Arc;
use tokio::sync::{mpsc, RwLock};
use tokio_stream::wrappers::UnboundedReceiverStream;
use futures::{FutureExt, StreamExt};
use serde::{Serialize, Deserialize};
use log::{info, debug};
use warp::ws::Message;
use prost::Message as ProstMessage;

use crate::backend_registry::BackendRegistry;
use crate::backend::{proto, BackendError};
use crate::fediventure;
use crate::player::Player;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
#[allow(non_snake_case)]
pub struct CharacterData {
    pub roomId: String,
    pub token: String,
    pub name: String,
    pub characterLayers: String,
    pub x: i32,
    pub y: i32,
    pub top: i32,
    pub bottom: i32,
    pub left: i32,
    pub right: i32
}

#[derive(Debug)]
pub struct Session {
    pub player: Arc<RwLock<Player>>,
    pub to_client: mpsc::UnboundedSender<Result<Message, warp::Error>>,
    pub to_backend: mpsc::UnboundedSender<fediventure::Message>,
}

impl Session {
    pub async fn new(to_client: mpsc::UnboundedSender<Result<Message, warp::Error>>, player: Arc<RwLock<Player>>, backend_registry: BackendRegistry) -> Result<Arc<Self>, BackendError> {
        let backend_url = std::env::var("BACKEND_URL").unwrap_or("http://localhost:50051".to_owned());
        let backend = backend_registry.get(&backend_url).await?;
        let this = Arc::new(Session {
            player: player,
            to_client,
            to_backend: backend.tx.clone()
        });

        backend.connect_client(this.clone()).await?;
        Ok(this)
    }

    pub fn send(&self, data: &[u8]) -> Result<(), mpsc::error::SendError<Result<Message, warp::Error>>> {
        self.to_client.send(Ok(Message::binary(data)))
    }

    pub async fn to_backend_message(&self, msg: proto::client_to_server_message::Message) -> Option<fediventure::Message> {
        use proto::client_to_server_message::Message;
        match msg {
            Message::UserMovesMessage(msg) => {
                let backend_id = self.player.read().await.id;
                if backend_id < 0 {
                    return None
                }

                let position = match msg.position {
                    Some(p) => p,
                    None => return None
                };

                Some(fediventure::PlayerMoved {
                    id: backend_id,
                    x: position.x,
                    y: position.y,
                }.into())
            }
            _ => None // TODO: Implement other message types
        }
    }

    pub async fn handle_frontend_message(&self, msg: Message) -> Result<(), mpsc::error::SendError<fediventure::Message>> {
        match proto::ClientToServerMessage::decode(msg.as_bytes()) {
            Ok(proto::ClientToServerMessage { message: Some(msg) }) => {
                if let Some(msg) = self.to_backend_message(msg).await {
                    return self.to_backend.send(msg)
                }
                Ok(())
            },
            #[allow(unused_must_use)]
            Ok(_) => {
                self.send(&[]);
                debug!("Received empty message from a client.");
                Ok(())
            }
            Err(e) => {
                debug!("Error while decoding a client message: {}", e);
                Ok(())
            }
        }
    }
}

pub async fn start(ws: warp::ws::WebSocket, character_data: CharacterData, backend_registry: BackendRegistry) {
    let (ws_tx, mut ws_rx) = ws.split();
    let (tx, rx) = mpsc::unbounded_channel();
    let rx = UnboundedReceiverStream::new(rx);

    tokio::task::spawn(rx.forward(ws_tx).map(|result| {
        if let Err(e) = result {
            info!("Websocket send error: {:?}", e)
        }
    }));

    let player = Arc::new(RwLock::new(Player::new(character_data.name, character_data.x, character_data.y)));
    let session = match Session::new(tx, player, backend_registry).await {
        Ok(session) => session,
        Err(e) => {
            info!("Error connecting to default backend: {:?}", e);
            return
        }
    };

    while let Some(result) = ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                info!("Websocket error: {:?}", e);
                break
            }
        };
        if let Err(e) = session.handle_frontend_message(msg).await {
            info!("Websocket send error: {:?}", e);
            break
        }
    }
}
