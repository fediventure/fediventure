use std::sync::Arc;
use tokio::sync::RwLock;
use crate::gateway::GatewayConnection;
use crate::fediventure;
use std::sync::atomic::{Ordering, AtomicI32};
use log::{warn, debug};

#[derive(Default)]
pub struct BackendImpl{
    gateways: RwLock<Vec<Arc<GatewayConnection>>>,
    next_id: AtomicI32,
}

impl BackendImpl {
    pub async fn register_gateway(&self, gateway: Arc<GatewayConnection>) {
        self.gateways.write().await.push(gateway);
    }

    pub async fn on_message(&self, msg: fediventure::Message, _from: Arc<GatewayConnection>) {
        use fediventure::Message::*;
        debug!("Received message: {:?}", msg);
        match msg {
            JoinPlayer(msg) => self.on_join_player(msg).await,
            PlayerMoved(msg) => self.on_player_moved(msg).await,
            _ => {
                warn!("Unhandled message: {:?}", msg)
            }
        }
    }

    async fn on_join_player(&self, msg: fediventure::JoinPlayer) {
        let id = self.next_id.fetch_add(1, Ordering::Relaxed);
        let player_joined: fediventure::Any = fediventure::PlayerJoined {
            id,
            name: msg.name
        }.into();

        for gw in self.gateways.read().await.iter() {
            gw.send(player_joined.clone());
        }
    }

    async fn on_player_moved(&self, msg: fediventure::PlayerMoved) {
        let msg: fediventure::Any = msg.into();
        for gw in self.gateways.read().await.iter() {
            gw.send(msg.clone());
        }
    }
}
