import Player from './player'

export default class Camera {
  public anchor: Player
  public readonly container: HTMLElement;
  public readonly content: HTMLElement;

  constructor(anchor: Player) {
    this.anchor = anchor;
    this.anchor.onmove = () => this.update();

    this.container = document.createElement('div');
    this.content = document.createElement('div');
    this.container.appendChild(this.content);

    this.container.className = 'camera-container';
    this.content.className = 'camera-content';
  }

  update() {
    this.content.style.left = `-${this.anchor.x}px`;
    this.content.style.top = `-${this.anchor.y}px`;
  }
}
