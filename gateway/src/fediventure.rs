use thiserror::Error;
use prost::Message as ProstMessage;
use std::collections::{HashMap, HashSet};

tonic::include_proto!("fediventure");
tonic::include_proto!("fediventure.player");
tonic::include_proto!("fediventure.player.movement");

pub static REQUIRED_EXTENSIONS: [&'static str; 2] = ["player", "playerMovement"];
pub static OPTIONAL_EXTENSIONS: [&'static str; 0] = [];

#[derive(Debug, Error)]
pub enum MessageDecodingError {
    #[error("Unsupported message type. Message id: {0}")]
    UnsupportedMessageId(i32),
    #[error("Unsupported message type. Message name: {0}")]
    UnsupportedMessageType(String),
    #[error("Error decoding message:")]
    DecodeError(#[from] prost::DecodeError),
}

#[derive(Debug, Default)]
pub struct IdMap {
    ids: HashMap<String, i32>,
    names: HashMap<i32, String>
}

impl IdMap {
    pub fn new(ids: Vec<MessageId>, extensions: Vec<String>) -> Self {
        let extensions: HashSet<String> = extensions.into_iter().collect();
        let mut this = IdMap::default();

        for message_id in ids {
            if !extensions.contains(&message_id.extension) {
                continue
            }

            this.names.insert(message_id.id, message_id.name.clone());
            this.ids.insert(message_id.name.clone(), message_id.id);
        }

        this
    }

    pub fn from_backend_msg(&self, msg: Any) -> Result<Message, MessageDecodingError> {
        if let Some(msg_type) = self.names.get(&msg.id) {
            parse_message(msg_type, msg.message.as_slice())
        } else {
            Err(MessageDecodingError::UnsupportedMessageId(msg.id))
        }
    }

    pub fn into_backend_msg(&self, msg: Message) -> Result<Any, MessageDecodingError> {
        if let Some(&id) = self.ids.get(msg.type_name()) {
            Ok(Any {
                id,
                message: msg.encode()
            })
        } else {
            Err(MessageDecodingError::UnsupportedMessageType(msg.type_name().to_owned()))
        }
    }
}

macro_rules! id_map {
    ($($ext:ident :: $msg:ident),+) => {
        #[derive(Debug)]
        pub enum Message {
            $(
                $msg($msg)
            ),+
        }

        impl Message {
            pub fn type_name(&self) -> &'static str {
                match self {
                    $(
                        Message::$msg(_) => stringify!($msg)
                    ),+
                }
            }

            pub fn encode(&self) -> Vec<u8> {
                match self {
                    $(
                        Message::$msg(msg) => {
                            let mut buf = Vec::with_capacity(msg.encoded_len());
                            msg.encode(&mut buf).unwrap();
                            buf
                        }
                    ),+
                }
            }
        }

        $(
            impl From<$msg> for Message {
                fn from(msg: $msg) -> Self {
                    Message::$msg(msg)
                }
            }
        )+

        fn parse_message(ty: &str, msg: &[u8]) -> Result<Message, MessageDecodingError> {
            Ok(match ty {
                $(
                stringify!($msg) => Message::$msg($msg::decode(msg)?),
                )+
                _ => Err(MessageDecodingError::UnsupportedMessageType(ty.to_owned()))?
            })
        }
    }
}

id_map! {
    player::JoinPlayer,
    player::PlayerJoined,
    player::PlayerLeft,
    playerMovement::PlayerMoved
}
