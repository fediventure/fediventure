This is a very incomplete and experimental fediventure frontend using native web technologies (i.e. stacked image tags instead of rendering everyting to a canvas).

Right now it only draws a map and scrolls around it. Feel free to hack.

Usage
-----

    docker build -t fediventure-front .
    docker run -p 5000:5000 --rm fediventure-front

Then point your browser at http://localhost:5000/.
