# Fediventure Overview

Welcome! This is the documentation for Fediventure, a project to bring an rc3-world like experience as a federated system across *spaces.

We base on [WorkAdventure](https://github.com/thecodingmachine/workadventure) and currently develop **multiple parallel approaches** to federate WorkAdventure. However, you can already **deploy your own Prototype instance for your \*space** and start inviting people over, working on maps, etc.

For further information about the Prototype, and future development of Fediventure, keep on reading. Otherwise, head on over to the [Prototype](prototype.md) section to see how you can get it running today.

## Prototype

This approach to Fediventure aims to get things running across *spaces as soon as possible. It currently mostly consists of working on the WorkAdventure JavaScript frontend, and a bare minimum of changes to the existing backend services so that you can host your own instance easily. To see how to get it running, head on over to the [Prototype](prototype.md) section.

## Future work: matrixless and matrixful implementations

We are currently designing two concurrent implementations of Fediventure that would provide a full federation featureset:

 - cross-*space authentication and identity
 - simplified *space discovery
 - protocol extendability for per-*space extentions
 - cross-*space moderation and safety

These are actively developed on our issue tracker:

 - [issues/1 - Matrixless federation protocol proposal](https://gitlab.com/fediventure/fediventure/-/issues/1)
 - issues/xxx - Matrixful federation protocol proposal (no issue, fixme!)

Once both approaches are fully fleshed out, a decision to follow either one will be followed, and the Prototype will slowly transition to become either implementation.
