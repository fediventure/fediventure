This is a prototype gateway for fediventure clients.

Right now it's intended to replace the pusher part, but it isn't a complete replacement yet.

Things it doesn't do:
  * It doesn't support authentication (even /anonymLogin, you have to use a pre-generated token)
  * It doesn't garbage collect dead connections, so if you disconnect, you have to restart the program
  * It doesn't support anything other than movement messages.

In other words—the basic architecture is done (but subject to change, feel free to change anything here), but basically nothing else.

How to use
----------

Start the program with e.g. `RUST_LOG=debug cargo run`, then connect to it with wabot or a workadventure client.
You can configure the backend it connects to by setting the BACKEND_URL environment variable. It defaults to `http://localhost:50051`.
