use std::pin::Pin;
use std::sync::Arc;
use std::convert::TryInto;
use futures::{Stream};
use tokio::sync::mpsc;
use tokio_stream::{StreamExt, wrappers::UnboundedReceiverStream};
use tonic::{transport::Server, Request, Response, Status, Streaming};
use log::{info, debug};

use fediventure::backend_server::{Backend, BackendServer};
use fediventure::{Empty, ClientHello, ServerHello, Metadata, Extension, extension::Presence};
use gateway::GatewayConnection;

pub mod fediventure;
pub mod gateway;
pub mod backend;

#[derive(Default)]
pub struct BackendApi(Arc<backend::BackendImpl>);

#[tonic::async_trait]
impl Backend for BackendApi {
    async fn get_metadata(&self, _request: Request<Empty>) -> Result<Response<Metadata>, Status> {
        let mut supported_extensions = Vec::with_capacity(fediventure::REQUIRED_EXTENSIONS.len() + fediventure::OPTIONAL_EXTENSIONS.len());
        for ext in &fediventure::REQUIRED_EXTENSIONS {
            supported_extensions.push(Extension {
                name: (*ext).to_owned(),
                presence: Presence::Required.into()
            });
        }
        for ext in &fediventure::OPTIONAL_EXTENSIONS {
            supported_extensions.push(Extension {
                name: (*ext).to_owned(),
                presence: Presence::Optional.into()
            });
        }

        Ok(Response::new(Metadata {
            supported_extensions
        }))
    }

    async fn hello(
        &self,
        request: Request<ClientHello>,
    ) -> Result<Response<ServerHello>, Status> {
        debug!("Hello called.");
        // TODO: Implement this function
        let request = request.into_inner();
        let mut missing_requirements: Vec<_> = fediventure::REQUIRED_EXTENSIONS.iter().collect();
        let mut choosen_extensions: Vec<fediventure::ChoosenExtension> = vec![];

        for ext in request.supported_extensions {
            if let Some(idx) = missing_requirements.iter().position(|req| **req == ext.name) {
                missing_requirements.remove(idx);
                choosen_extensions.push(fediventure::ChoosenExtension {
                    name: ext.name.clone()
                });
            } else if fediventure::OPTIONAL_EXTENSIONS.contains(&ext.name.as_str()) {
                choosen_extensions.push(fediventure::ChoosenExtension {
                    name: ext.name.clone()
                });
            }
        }

        let reply = ServerHello {
            extensions: choosen_extensions,
            message_ids: fediventure::MESSAGE_IDS.clone()
        };
        Ok(Response::new(reply))
    }

    type CommunicateStream = Pin<Box<dyn Stream<Item = Result<fediventure::Any, Status>> + Send + Sync + 'static>>;

    async fn communicate(&self, request: Request<Streaming<fediventure::Any>>) -> Result<Response<Self::CommunicateStream>, Status> {
        let (gateway_tx, gateway_rx) = mpsc::unbounded_channel();
        let gateway_connection = Arc::new(GatewayConnection::new(&request, gateway_tx));
        self.0.register_gateway(gateway_connection.clone()).await;

        let backend_impl = self.0.clone();
        tokio::task::spawn(async move {
            let mut request = request.into_inner();
            while let Some(Ok(msg)) = request.next().await {
                match msg.try_into() {
                    Ok(msg) => backend_impl.on_message(msg, gateway_connection.clone()).await,
                    Err(err) => {
                        // TODO: Send feedback about the error
                        // TODO: Also, close the connection
                        // For now we just ignore the message
                        info!("Received an invalid message: {:#}", err);
                    }
                }
            }
        });

        let output_stream = UnboundedReceiverStream::new(gateway_rx).map(|item| Ok(item));
        Ok(Response::new(Box::pin(output_stream)))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    pretty_env_logger::init();

    let addr = "0.0.0.0:50051".parse().unwrap();
    let backend = BackendApi::default();

    println!("Listening on {}", addr);

    Server::builder()
        .add_service(BackendServer::new(backend))
        .serve(addr)
        .await?;

    Ok(())
}
